const applicationId = "E5E73985"; // This is the AppId
var currentMediaURL = "https://fillyradio.com/stream-320k"; // Hardcoded the stream URL here
var contentType = "audio/mpeg"; // The expected MIME Type

// Init some variables
var castSession = false;
var playerController = false;
var castState = false;
// playerState is init as an object, so we can set the value
// 'playerState.value = "PLAYING";' to it, if media is playing
// when we create a new session (I feel we could do better, though)

// Determines if we start playback as soon as the Chromecast connects.
// The idea is that the user doesn't need to start playback again
// if it was already playing in the browser.
var wasPlaying = false;

// This updates the state of the #playback button
var remoteToLocalState = function(state) {
  state === "BUFFERING" ? $(".playButton").html("Buffering") : null;
  state === "PLAYING" ? $(".playButton").html("Stop") : null;
  state === "LOADING" ? $(".playButton").html("Loading") : null;
  state === "Ready" ? $(".playButton").html("Click to Cast") : null;
  !state ? $(".playButton").html("Play") : null;
};

// Initializing the Cast API
initializeCastApi = function() {
  var castContext = cast.framework.CastContext.getInstance();
  castContext.setOptions({
    receiverApplicationId: applicationId,
    autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
  });
  // Add an event listener so we can get the Cast State, and make things
  // happen depending on the state
  var castStateChanged = cast.framework.CastContextEventType.CAST_STATE_CHANGED;
  castContext.addEventListener(castStateChanged, function(event) {
    // Put the state to the global variable/object
    castState = event.castState;
    if (event.castState === "CONNECTING") {
      remoteToLocalState("LOADING");
      if (!audio.paused) {
        stopPlayback();
        wasPlaying = true;
      }
    } else if (event.castState === "CONNECTED") {
      // put the castSession to the global variable so we can call it elsewhere
      castSession = cast.framework.CastContext.getInstance().getCurrentSession();
      console.log(castSession);

      // Start playback if playback was already happening in the browser.
      if (wasPlaying) {
        startCast();
      }
      // TODO: Find out why I have to put a timeout on this function, and
      // replace it with a ".then()" if possible.
      setTimeout(checkMediaSession, 1000);
    } else if (event.castState === "NOT_CONNECTED") {
      $(".playButton").tooltip("hide");
      remoteToLocalState();
    }
  });
};

// This starts the media playback
var startCast = function() {
  wasPlaying = false;
  // Here we set the media parameters, and seed the initial metadata state
  // The cast app we are using gets it's own metadata once the session starts,
  // but won't apply that to the player unless it's playing.
  // The DB event (firebase realtime database '.on("value" function())') fires
  // BEFORE there is a media session, so we need to seed this data, then it
  // can take care of itself
  var mediaInfo = new chrome.cast.media.MediaInfo(currentMediaURL, contentType);
  mediaInfo.streamType = chrome.cast.media.StreamType.LIVE;
  mediaInfo.metadata = new chrome.cast.media.MusicTrackMediaMetadata();
  mediaInfo.metadata.title = metadata.track;
  mediaInfo.metadata.artist = metadata.artist;
  mediaInfo.metadata.albumName = metadata.album;
  coverUrl = metadata.coverUrl;
  mediaInfo.metadata.images = [new chrome.cast.Image(coverUrl)];

  // bundle the above block into a LoadRequest
  var request = new chrome.cast.media.LoadRequest(mediaInfo);

  // Now we tell the Chromecast to load this request. The Chromecast takes
  // care of handling the data, we just wait for a promise that says it
  // succeeded.
  castSession.loadMedia(request).then(
    function() {
      $(".playButton").tooltip("hide");
      // We create a playerController here, so we can watch and control the
      // player on the Sender app
      var player = new cast.framework.RemotePlayer();
      playerController = new cast.framework.RemotePlayerController(player);

      // We initialize an Event listener, to handle the UI and allow us to
      // use the state data elsewhere
      playerController.addEventListener(
        cast.framework.RemotePlayerEventType.PLAYER_STATE_CHANGED,
        function(event) {
          remoteToLocalState(event.value);
          playerState = event;
        }
      );
    },
    function(errorCode) {
      console.log("Error code: " + errorCode);
    }
  );
};

// This checks to see if there is an exisiting mediaSession in the
// Chromecast if we re-connect to the castSession
function checkMediaSession() {
  console.log("checked media session");
  if (castSession.getMediaSession() !== null) {
    playerState.value = "PLAYING";
    remoteToLocalState("PLAYING");
    var player = new cast.framework.RemotePlayer();
    playerController = new cast.framework.RemotePlayerController(player);
    createPlayerListener();
  } else {
    console.log("no media session");
    $(".playButton").tooltip("show");
    remoteToLocalState();
  }
}

// This allows us to see what the state of the remotePlayer
// is, however, it only runs when the state changes on the remote,
// so we have to work around not knowing the state beforehand.

// TODO
// I need to read up on this, as I am sure there is a way to
// get the state without waiting for it to change on the Chromecast...
function createPlayerListener() {
  playerController.addEventListener(
    cast.framework.RemotePlayerEventType.PLAYER_STATE_CHANGED,
    function(event) {
      remoteToLocalState(event.value);
      playerState = event;
    }
  );
}
