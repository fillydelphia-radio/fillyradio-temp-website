var audioMetadata = {};
var metadata = {};

if (
  "mediaSession" in navigator ||
  (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform))
) {
  jQuery(".volumeBar").remove();
} else if ("mediaSession" in navigator) {
  jQuery(".volumeBar").remove();
}

var config = {
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
  projectId: "fillydelphia-radio"
};
function htmlDecode(input) {
  var e = document.createElement("div");
  e.innerHTML = input;
  // handle case of empty input
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
let audio = document.createElement("audio");
firebase.initializeApp(config);
var database = firebase.database();
var nowPlaying = firebase.database().ref("rest/fillydelphia-radio/now-playing");

nowPlaying.on("value", function(snapshot) {
  audioMetadata = {
    artist: processMeta(snapshot).artist,
    title: processMeta(snapshot).title,
    album: processMeta(snapshot).album
  };
  metadata.artist = snapshot.val().artist
    ? htmlDecode(snapshot.val().artist)
    : null;
  metadata.album = snapshot.val().album
    ? htmlDecode(snapshot.val().album)
    : null;
  metadata.track = snapshot.val().title
    ? htmlDecode(snapshot.val().title)
    : null;
  metadata.coverUrl =
    "https://fillyradio.com/cover?nocache=" + new Date().getTime();
  evaluateViewer(audioMetadata);
  setMetaText(audioMetadata);
  if ("mediaSession" in navigator && !audio.paused) {
    updateMetadata(
      audioMetadata.title,
      audioMetadata.artist,
      audioMetadata.album
    );
  }
});

function evaluateViewer(m) {
  audio.title = m.title + " - " + m.artist;
  if ("mediaSession" in navigator && !audio.paused) {
    setDOMTitle(m.title, m.artist);
    updateMetadata(m.title, m.artist, m.album);
  } else if (!audio.paused) {
    setDOMTitle(m.title, m.artist);
  }
}

function setMetaText(m) {
  jQuery(".artist").html(m.artist);
  jQuery(".title").html(m.title);
  jQuery(".album").html(m.album);
  axios
    .get("https://fillyradio.com/cover/300?" + new Date().getTime(), {
      responseType: "blob"
    })
    .then(function(response) {
      var reader = new window.FileReader();
      reader.readAsDataURL(response.data);
      reader.onload = function() {
        var imageDataUrl = reader.result;
        jQuery(".cover").attr("src", imageDataUrl);
      };
    })
    .catch(function(e) {
      console.log(e);
      jQuery(".cover").attr(
        "src",
        "https://res.cloudinary.com/stormdragon-designs/image/upload/c_scale,q_auto,w_300,h_300/v1526676139/fricon_600-600_zersgq.png"
      );
    });
}

function onPlayButtonClick() {
  if (
    castState === "NOT_CONNECTED" ||
    castState === "NO_DEVICES_AVAILABLE" ||
    !castState
  ) {
    localPlayback();
  } else if (castState === "CONNECTED") {
    remotePlayback();
  } else if (castState === "CONNECTING") {
  }
}

function stopPlayback() {
  audio.pause();
  audio.src =
    "https://f000.backblazeb2.com/file/fillydelphia-radio/500-milliseconds-of-silence.mp3";
  audio.play();
}

function playAudio() {
  audio.src = "https://fillyradio.com/stream-128k";
  audio.volume = 0.5;
  audio
    .play()
    .then(_ => {
      audio.title = audioMetadata.title + " - " + audioMetadata.artist;
      setDOMTitle(audioMetadata.title, audioMetadata.artist);
      if ("mediaSession" in navigator) {
        updateMetadata(
          audioMetadata.title,
          audioMetadata.artist,
          audioMetadata.album
        );
      }
      jQuery(".playButton").html("Pause");
    })
    .catch(error => console.log(error));
}

audio.addEventListener("ended", function() {
  jQuery(".playButton").html("Play");
  document.title = "Fillydelphia Radio";
});

jQuery(".playButton").click(onPlayButtonClick);

function processMeta(snapshot) {
  var results = snapshot.val();
  if (results.title != null) {
    var track = results.title;
  } else {
    var track = "Unknown Track";
  }
  if (results.artist != null) {
    var artist = results.artist;
  } else {
    var artist = "Unknown Artist";
  }
  if (results.album != null && results.album != results.title) {
    var album = results.album;
  } else if (results.album == results.title) {
    var album = results.album + " - Single";
  } else {
    var album = "Unknown Album";
  }
  return { title: track, artist: artist, album, album };
}

function updateMetadata(track, artist, album) {
  navigator.mediaSession.setActionHandler("pause", function() {
    stopPlayback();
  });

  navigator.mediaSession.setActionHandler("play", function() {
    playAudio();
  });

  navigator.mediaSession.metadata = new MediaMetadata({
    title: track.replace("&#39;", "'"),
    artist: artist.replace("&#39;", "'"),
    album: album.replace("&#39;", "'"),
    artwork: [
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "96x96",
        type: "image/png"
      },
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "128x128",
        type: "image/png"
      },
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "192x192",
        type: "image/png"
      },
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "256x256",
        type: "image/png"
      },
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "384x384",
        type: "image/png"
      },
      {
        src: "https://fillyradio.com/cover?" + new Date().getTime(),
        sizes: "512x512",
        type: "image/png"
      }
    ]
  });
}

function setDOMTitle(track, artist) {
  document.title =
    htmlDecode(track) + " - " + htmlDecode(artist) + " | Fillydelphia Radio";
}

function localPlayback() {
  if (audio.paused) {
    if ("connection" in navigator) {
      if (navigator.connection.type === "cellular") {
        alert(
          "You're not connected to WiFi. You might get some nasty bills. Be careful!"
        );
      }
    }
    playAudio();
    jQuery(".playButton").html("Buffering");
  } else {
    stopPlayback();
    jQuery(".playButton").html("Paused");
    document.title = "Fillydelphia Radio";
  }
}

var remotePlayback = function() {
  if (!audio.paused) {
    stopPlayback();
  }
  if (!playerState.value || !playerController) {
    remoteToLocalState("LOADING");
    startCast();
    console.log("I started the stream");
  } else {
    playerController.stop();
    playerState = false;
    console.log("I set false");
  }
};
