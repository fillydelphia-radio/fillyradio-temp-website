self.importScripts('https://www.gstatic.com/firebasejs/5.0.1/firebase-app.js', 'https://www.gstatic.com/firebasejs/5.0.1/firebase-database.js');
var config = {
    databaseURL: "https://fillydelphia-radio.firebaseio.com",
    projectId: "fillydelphia-radio"
};

let navigator;
firebase.initializeApp(config);
var database = firebase.database();
var nowPlaying = firebase.database().ref('rest/fillydelphia-radio/now-playing');
nowPlaying.on('value', function (snapshot) {
    postMessage(snapshot);
});